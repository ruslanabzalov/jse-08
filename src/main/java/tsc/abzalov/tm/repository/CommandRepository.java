package tsc.abzalov.tm.repository;

import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.constant.TaskManagerUnit;
import tsc.abzalov.tm.model.Command;

import static java.util.Arrays.stream;
import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.constant.TaskManagerUnit.ARGUMENT;
import static tsc.abzalov.tm.constant.TaskManagerUnit.COMMAND;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            CMD_HELP, ARG_HELP, "Shows all available commands."
    );

    private static final Command INFO = new Command(
            CMD_INFO, ARG_INFO, "Shows CPU and RAM info."
    );

    private static final Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT, "Shows developer info."
    );

    private static final Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION, "Shows application version."
    );

    private static final Command COMMANDS = new Command(
            CMD_COMMANDS, ARG_COMMANDS, "Show all available commands."
    );

    private static final Command ARGUMENTS = new Command(
            CMD_ARGUMENTS, ARG_ARGUMENTS, "Show all available arguments."
    );

    private static final Command EXIT = new Command(
            CMD_EXIT, null, "Shutdowns application."
    );

    private static final Command[] ALL_COMMANDS = { HELP, INFO, ABOUT, VERSION, COMMANDS, ARGUMENTS, EXIT };

    @Override
    public Command[] getCommands() {
        return ALL_COMMANDS;
    }

    @Override
    public String[] getCommandNames() {
        return getParamsByUnit(COMMAND);
    }

    @Override
    public String[] getCommandArgs() {
        return getParamsByUnit(ARGUMENT);
    }

    private String[] getParamsByUnit(TaskManagerUnit unit) {
        final Command[] filteredCommands = (unit.equals(COMMAND))
                ? stream(ALL_COMMANDS)
                    .filter(command -> !(command.getName() == null || command.getName().isEmpty()))
                    .toArray(Command[]::new)
                : stream(ALL_COMMANDS)
                    .filter(command -> !(command.getArg() == null || command.getArg().isEmpty()))
                    .toArray(Command[]::new);
        final String[] resultParams = new String[filteredCommands.length];
        String currentParam;

        for (int i = 0; i < filteredCommands.length; i++) {
            currentParam = (unit.equals(COMMAND)) ? filteredCommands[i].getName() : filteredCommands[i].getArg();
            resultParams[i] = currentParam;
        }

        return resultParams;
    }

}
